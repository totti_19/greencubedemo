var gulp = require('gulp'),
        uglify = require('gulp-uglify'),
        concat = require('gulp-concat'),
        minify = require('gulp-minify-css');


gulp.task('mainTask', function () {
    gulp.src(['public/js/**/*.core.js'])
            .pipe(concat('core.min.js'))
            .pipe(uglify())
            .pipe(gulp.dest('public/dist/js/'));
    gulp.src(['public/js/**/*.product.js'])
            .pipe(concat('product.min.js'))
            .pipe(uglify())
            .pipe(gulp.dest('public/dist/js/'));
    gulp.src('public/css/**/*.core.css')
            .pipe(minify())
            .pipe(concat('style.core.min.css'))
            .pipe(gulp.dest('public/dist/css'));
});

gulp.task('default', function () {
    console.log('Gulp is running ok!');
});