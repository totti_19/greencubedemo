# Proyecto demo greencube

## Instalación de dependencias
> npm install

## Generacion de archivos de distribucion con Gulp
> gulp mainTask

## Uso de lean() mongoose

> Cuando se realiza una consulta con el tipo comun find de mongoose y la funcion exec, sin lean()
se retorna un documento MongoDb, con todos sus atributos, metodos setters y getters, lo que implica una carga
de datos a la consulta; ahora bien esto no sucede si usamos lean() aplicado a la consulta, dado a que con esto
se retorna un simple objeto plano Javascript, sin sus atributos y demas cosas mencionadas, por lo que la carga de 
datos se reduce y por ende se mejora el tiempo de respuesta respecto al anterior, y mucho mejor, cuando el O(n) 
tiende a incrementar.

### Geovanny Quirós Pérez | Fullstack developer
#### 2017