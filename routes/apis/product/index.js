var express = require('express');
var router = express.Router();
var productController = require('../../../apis/controllers/product.controller.server');

router.get('/', function (req, res, next) {
    res.json({status: ":)", module: 'products'});
});
router.get('/:id', function (req, res, next) {
    var productId = req.params.id;
    productController.getById(productId, function (product) {
        res.json({productResponse: product});
    });
});
router.post('/product.json', function (req, res) {
    var description = req.body.description;
    var product = {
        description: description
    };
    productController.insert(product,
            function (productResponse) {
                res.json({productResponse: productResponse});
            }
    );
});
router.get('/:currentPage/:limit/product.json', function (req, res, next) {
    var currentPage = req.params.currentPage;
    var limit = req.params.limit;
    productController.paginate(currentPage, limit,
            function (products) {
                res.json({productResponse: products});
            }
    );
});
module.exports = router;

