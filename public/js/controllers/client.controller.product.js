angular.module('greencubedemo')
        .controller("productController", ['$scope', 'productService',
            function ($scope, productService) {

                /* var initialization */
                $scope.currentPage = 1;
                $scope.limit = 10;
                
                $scope.product = {
                    description: ''
                };
                
                var optionsSnackbar = {
                    content: '',
                    style: 'toast',
                    timeout: 0
                };
                /* end var initialization */
                
                $scope.getProducts = function (currentPage, limit) {
                    productService.get({
                        id: null,
                        currentPage: currentPage,
                        limit: limit
                    }, function (data) {
                        $scope.products = data.productResponse;
                        $scope.nextDisable = data.productResponse.length < limit;
                    });
                };

                $scope.previous = function () {
                    $scope.currentPage--;
                    $scope.getProducts($scope.currentPage, $scope.limit);
                };

                $scope.next = function () {
                    $scope.currentPage++;
                    $scope.getProducts($scope.currentPage, $scope.limit);
                };

                $scope.save = function () {
                    productService.save($scope.product, function (data) {
                        if(data.productResponse){
                            $scope.product.description = '';
                            optionsSnackbar.content = '¡Producto agregado exitosamente!';
                            optionsSnackbar.timeout = 1500;
                            showSnackbar();
                        }
                    });
                };
                
                var showSnackbar = function () {
                    $.snackbar(optionsSnackbar);
                    setTimeout(function () {
                        $("#snackbarForm").snackbar("hide");
                    }, 1000);
                };
            }
        ]);