angular.module('greencubedemo')
        .config(['$stateProvider', '$urlRouterProvider',
            function ($stateProvider, $urlRouterProvider) {
                $stateProvider
                        .state('product', {
                            url: '/product',
                            abstract: true,
                            template: '<ui-view>',
                            controller: "productController"
                        })
                        .state("product.add", {
                            url: "/add",
                            templateUrl: "views/product.add.html"
                        })
                        .state("product.list", {
                            url: "/list",
                            templateUrl: "views/product.list.html"
                        });
            }
        ]);
