angular.module('greencubedemo')
        .factory('productService', ['$resource',
            function ($resource) {
                return $resource(enviroment.urlApi(),
                                {
                                    id:'@_id',
                                    currentPage:'@_currentPage',
                                    limit:'@_limit'
                                }
                );
            }
        ]);