var productController = {
    productModel: require('../models/product.model.server'),
    getById: function (productId, callback) {
        this.productModel.findById(productId,
                function (err, product) {
                    if (err) {
                        console.log(err);
                    } else {
                        callback(product);
                    }
                }
        );
    },
    insert: function (product, callback) {
        var productToSave = new this.productModel(product);
        productToSave.save(function (err, productResponse) {
            if (err) {
                console.error(err);
                return;
            }
            callback(productResponse);
        });
    },
    paginate: function (currentPage, limit, callback) {

        limit       = parseInt(limit);
        currentPage = parseInt(currentPage);
        var skip    = limit * (currentPage - 1);

        this.productModel.find({})
                .skip(skip)
                .limit(limit)
                .lean()
                .exec(function (err, products) {
                    if (err) {
                        console.log(err);
                        return;
                    }
                    callback(products);
                });
    }
};

module.exports = productController;

