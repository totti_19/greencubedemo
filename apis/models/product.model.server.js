var mongoose = require('mongoose')
        , Schema = mongoose.Schema;

var productSchema = new Schema({
    description: String
});

module.exports = mongoose.model('product', productSchema);